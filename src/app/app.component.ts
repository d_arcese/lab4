import { Component } from '@angular/core';
import { CoursesComponent } from './courses/courses.component.ts'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hello Angular!';
}
