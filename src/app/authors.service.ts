import { Injectable } from '@angular/core';

@Injectable()
export class AuthorsService {
  authors=["Daniela Arcese"];
  constructor() { }
  getAuthors(): string[] {
      return this.authors;
  }
}
