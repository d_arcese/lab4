import { Injectable } from '@angular/core';

@Injectable()
export class CourseService {
  courses=["Making Spaghetti 101","Making Brownies 102","Making Doritos 404"];
  constructor() { }
  getCourses(): string[] {
      return this.courses;
  }
  addCourse(newcourse: string) {
    
    if (newcourse) {
      this.courses.push(newcourse);
    }
  }
  
}
