import { Component, OnInit } from '@angular/core';
import { CourseService } from '../courses.service';
import {AutoGrowDirective} from '../auto-grow.directive';
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
  providers: [CourseService]
})
export class CoursesComponent implements OnInit {
  title='Course title page';
  courses;
  constructor(courseService: CourseService, private service:CourseService) {
    this.courses=courseService.getCourses();
  }
  addCourse(newcourse: string) {
    this.service.addCourse(newcourse);
  }
  ngOnInit() {
  }
  
}
